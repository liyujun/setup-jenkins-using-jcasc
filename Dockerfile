FROM jenkins/jenkins:latest

COPY plugins.yml .

# 跳过引导步骤
ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false
# 安装插件
RUN jenkins-plugin-cli --plugin-file plugins.yml
# 配置 JCasC
ENV CASC_JENKINS_CONFIG ~/casc_configs
COPY casc/* ~/casc_configs/
