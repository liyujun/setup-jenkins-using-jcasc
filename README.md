# Setup jenkins using JCasC

```bash
docker pull liyujun/jenkins:latest
docker run --rm -p 8080:8080 \
  --env JENKINS_ADMIN_ID=admin \
  --env JENKINS_ADMIN_PASSWORD=password \
  liyujun/jenkins:latest
```

